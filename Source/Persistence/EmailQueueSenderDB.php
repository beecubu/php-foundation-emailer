<?php

namespace Beecubu\Foundation\Emailer\Persistence;

use Beecubu\Foundation\Emailer\Entities\EmailQueueItem\EmailQueueItem;
use Beecubu\Foundation\MongoDB\DBConnection\DBConnection;
use Beecubu\Foundation\MongoDB\Driver\MongoCollection;
use MongoDB\BSON\ObjectId;

/**
 * Gestiona la cola de correos.
 */
class EmailQueueSenderDB extends DBConnection
{
    /** @var EmailQueueSenderDB|null */
    protected static $instance = null;

    /**
     * Devuelve el repo actual.
     *
     * @return static El repo actual.
     */
    public static function current(): EmailQueueSenderDB
    {
        // first time?
        if ( ! self::$instance)
        {
            self::$instance = new static();
        }
        return self::$instance;
    }

    /**
     * Obtiene el primer email a enviar.
     *
     * @param string $state El estado a filtrar.
     *
     * @return EmailQueueItem El item obtenido.
     */
    public function firstEmailQueueItemWithState(string $state): ?EmailQueueItem
    {
        if ($rawData = $this->db()->findOne(['state' => $state]))
        {
            return EmailQueueItem::instanceWithRawData($rawData);
        }
        return null;
    }

    /**
     * Retorna la BD a fer servir.
     *
     * @return MongoCollection
     */
    protected function db(): MongoCollection
    {
        return $this->db->emails_queue;
    }

    /**
     * Guarda un email a la BD.
     *
     * @param EmailQueueItem $emailQueueItem L'ítem a guardar.
     */
    public function saveEmailQueueItem(EmailQueueItem $emailQueueItem): void
    {
        $rawData = $emailQueueItem->rawData();
        // save this
        if ($this->db()->save($rawData))
        {
            $emailQueueItem->set_ivar('id', $rawData->_id);
        }
    }

    /**
     * Elimina un email que està a la llista d'espera d'enviaments.
     *
     * @param EmailQueueItem $emailQueueItem L'email a esborrar.
     */
    public function deleteEmailQueueItem(EmailQueueItem $emailQueueItem): void
    {
        $this->db()->remove(['_id' => new ObjectId($emailQueueItem->id)]);
    }
}
