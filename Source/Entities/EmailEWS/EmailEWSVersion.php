<?php

namespace Beecubu\Foundation\Emailer\Entities\EmailEWS;

use Beecubu\Foundation\Core\Enum;
use jamesiarmes\PhpEws\Client;

/**
 * Versió del servidor Exchange.
 */
class EmailEWSVersion extends Enum
{
    public const VERSION_2007     = Client::VERSION_2007;
    public const VERSION_2007_SP1 = Client::VERSION_2007_SP1;
    public const VERSION_2009     = Client::VERSION_2009;
    public const VERSION_2010     = Client::VERSION_2010;
    public const VERSION_2010_SP1 = Client::VERSION_2010_SP1;
    public const VERSION_2010_SP2 = Client::VERSION_2010_SP2;
    public const VERSION_2013     = Client::VERSION_2013;
    public const VERSION_2013_SP1 = Client::VERSION_2013_SP1;
    public const VERSION_2016     = Client::VERSION_2016;
}
