<?php

namespace Beecubu\Foundation\Emailer\Entities\EmailEWS;

use jamesiarmes\PhpEws\Client;

/**
 * Client EWS tunejat.
 */
class EmailEWSClient extends Client
{
    /**
     * Configura si s'ha de verificar el servidor o no.
     *
     * @param bool $value TRUE = Verifica el servidor, FALSE = no verifica una merda.
     */
    public function setVerifyPeer($value)
    {
        $this->curl_options[CURLOPT_SSL_VERIFYHOST] = $value;
        $this->curl_options[CURLOPT_SSL_VERIFYPEER] = $value;
    }
}
