<?php

namespace Beecubu\Foundation\Emailer\Entities\Email;

use Beecubu\Foundation\Emailer\Entities\EmailConfig\EmailConfig;
use Beecubu\Foundation\Emailer\Entities\EmailConfig\EmailConfigService;
use Beecubu\Foundation\Emailer\Entities\EmailEWS\EmailEWSClient;
use Beecubu\Foundation\Emailer\Entities\EmailQueueItem\EmailQueueItem;
use Beecubu\Foundation\Emailer\Entities\Exceptions\EmailEWSSendingError;
use Exception;
use jamesiarmes\PhpEws\ArrayType\ArrayOfRecipientsType;
use jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAllItemsType;
use jamesiarmes\PhpEws\Enumeration\BodyTypeType;
use jamesiarmes\PhpEws\Enumeration\MessageDispositionType;
use jamesiarmes\PhpEws\Enumeration\ResponseClassType;
use jamesiarmes\PhpEws\Request\CreateItemType;
use jamesiarmes\PhpEws\Type\BodyType;
use jamesiarmes\PhpEws\Type\EmailAddressType;
use jamesiarmes\PhpEws\Type\MessageType;
use jamesiarmes\PhpEws\Type\SingleRecipientType;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email as SymfonyEmail;

/**
 * Envia un email.
 */
class Email
{
    /** @var EmailConfig|null */
    private $ivar_config = null;
    /** @var Mailer|null */
    private $ivar_mailer = null;
    /** @type EmailEWSClient $ivar_ews */
    private $ivar_ews = null;

    // Class constructors

    /**
     * Crea un nou email configurat amb un Email Config concret.
     *
     * @param EmailConfig $config La configuració a utilitzar.
     *
     * @return Email El mail configurat.
     */
    public static function emailWithEmailConfig(EmailConfig $config): self
    {
        $mail = new self();
        // assign the config
        $mail->ivar_config = $config;
        // is an EWS config?
        if ($config->service === EmailConfigService::EWS)
        {
            $mail->ivar_ews = new EmailEWSClient($config->server, $config->version);
            $mail->ivar_ews->authWithUserAndPass($config->userName, $config->password);
            $mail->ivar_ews->setVerifyPeer($config->verifyPeer);
        }
        else // init the Swift mailer
        {
            $mail->ivar_mailer = new Mailer
            (
                (new EsmtpTransport($config->server, $config->port))
                    ->setLocalDomain($config->localDomain())
                    ->setUsername($config->userName)
                    ->setPassword($config->password)
            //->setAuthMode($config->authMode)
            //->setEncryption($config->encryption)
            //->setStreamOptions(['ssl' => ['allow_self_signed' => $config->verifyPeer === false, 'verify_peer' => ($config->verifyPeer !== false)]])
            );
        }

        return $mail;
    }

    // Public methods

    /**
     * Fa una prova de connexió al servidor SMTP.
     *
     * @return mixed TRUE = Tot ok, si falla retorna un string amb l'error.
     */
    public function test(): bool
    {
        try
        {
            //$this->ivar_mailer->getTransport()->start();
            //$this->ivar_mailer->getTransport()->stop();
            // all seems to be ok
            return true;
        }
        catch (Exception $e)
        {
            return $e->getMessage();
        }
    }

    /**
     * Envia un correu a una persona externa a la plataforma.
     *
     * @param string $ownerId El propietari.
     * @param string $to L'e-mail a on enviar el correu.
     * @param string $toName El nom del receptor.
     * @param string $subject L'assumpte del missatge.
     * @param string $body El cos del missatge.
     * @param bool $queue TRUE = Posa en qua l'e-mail, FALSE = l'envia al moment.
     * @param EmailAttachment[] $attachments Llistat de fitxers adjunts.
     * @param ?string $replyTo El reply to.
     * @param mixed $data Informació adicional.
     *
     * @throws EmailEWSSendingError
     * @throws TransportExceptionInterface
     */
    public function send(string $ownerId, string $to, string $toName, string $subject, string $body, bool $queue = true, array $attachments = [], ?string $replyTo = null, $data = null): void
    {
        if ($queue)
        {
            EmailQueueItem::emailQueueItemWithDataInfo($data, $ownerId, $to, $toName, $subject, $body, $attachments, $replyTo)->save();
        }
        else // send it right now!
        {
            if ($this->ivar_config->service === EmailConfigService::EWS)
            {
                // Build the request.
                $request = new CreateItemType();
                $request->Items = new NonEmptyArrayOfAllItemsType();
                // Save the message, and send it.
                $request->MessageDisposition = MessageDispositionType::SEND_AND_SAVE_COPY; // SEND_ONLY;
                // Create the message.
                $message = new MessageType();
                $message->Subject = $subject;
                $message->ToRecipients = new ArrayOfRecipientsType();
                // Set the sender.
                $message->From = new SingleRecipientType();
                $message->From->Mailbox = new EmailAddressType();
                $message->From->Mailbox->EmailAddress = $this->ivar_config->userName;
                // Set the recipient.
                $recipient = new EmailAddressType();
                $recipient->Name = $toName;
                $recipient->EmailAddress = $to;
                $message->ToRecipients->Mailbox[] = $recipient;
                // Set the message body.
                $message->Body = new BodyType();
                $message->Body->BodyType = BodyTypeType::HTML;
                $message->Body->_ = $body;
                // Add the message to the request.
                $request->Items->Message[] = $message;
                // Send the email(s).
                $response = $this->ivar_ews->CreateItem($request);
                // Iterate over the results, printing any error messages or message ids.
                $response_messages = $response->ResponseMessages->CreateItemResponseMessage;
                foreach ($response_messages as $response_message)
                {
                    $errors = [];
                    // Make sure the request succeeded.
                    if ($response_message->ResponseClass != ResponseClassType::SUCCESS)
                    {
                        $code = $response_message->ResponseCode;
                        $message = $response_message->MessageText;
                        $errors[] = "Message failed to create with \"$code: $message\"";
                    }
                    // throws an error
                    if (count($errors) !== 0) throw new EmailEWSSendingError($errors);
                }
            }
            else // Swift Mailer service
            {
                $email = (new  SymfonyEmail())
                    ->from(new Address($this->ivar_config->from, $this->ivar_config->fromName))
                    ->to(new Address($to, $toName))
                    ->subject($subject)
                    ->html($body);
                // append reply to
                if ($replyTo)
                {
                    $email->replyTo($replyTo);
                }
                // append attachments
                foreach ($attachments as $attachment)
                {
                    $email->attach($attachment->data, $attachment->name, $attachment->mime);
                }
                // send the email
                $this->ivar_mailer->send($email);
            }
        }
    }

    /**
     * Envia un correu que està a la cua.
     *
     * @param EmailQueueItem $email L'email a enviar.
     *
     * @throws EmailEWSSendingError
     * @throws TransportExceptionInterface
     */
    public function sendQueuedEmail(EmailQueueItem $email): void
    {
        $this->send
        (
            $email->ownerId,
            $email->to,
            $email->toName ?? '',
            $email->subject ?? '',
            $email->body ?? '',
            false,
            $email->attachments,
            $email->replyTo
        );
    }
}
