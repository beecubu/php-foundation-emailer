<?php

namespace Beecubu\Foundation\Emailer\Entities\Email;

use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\MongoDB\Entity;

/**
 * Representa un adjunt d'un email.
 *
 * @property-read string $name El nom de l'adjunt.
 * @property-read string $data El contingut de l'adjunt.
 * @property-read string $mime El tipus del contingut.
 */
class EmailAttachment extends Entity
{
    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'name' => [Property::READ_ONLY, Property::IS_STRING],
            'data' => [Property::READ_ONLY, Property::IS_STRING],
            'mime' => [Property::READ_ONLY, Property::IS_STRING],
        ];
    }

    protected function getData(): string
    {
        return base64_decode($this->ivars['data'] ?? '');
    }

    // Public methods

    /**
     * Crea un nou adjunt.
     *
     * @param string $name El nom.
     * @param string $data El contingut.
     * @param string $mime El tipus de contingut.
     */
    public static function create(string $name, string $data, string $mime): self
    {
        $obj = new static();
        $obj->set_ivar('name', $name);
        $obj->set_ivar('data', base64_encode($data));
        $obj->set_ivar('mime', $mime);
        return $obj;
    }
}
