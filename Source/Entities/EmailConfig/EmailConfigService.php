<?php

namespace Beecubu\Foundation\Emailer\Entities\EmailConfig;

use Beecubu\Foundation\Core\Enum;

/**
 * Tipus de servidor de correu.
 */
class EmailConfigService extends Enum
{
    public const SMTP = 'smtp'; // Simple Mail Transfer Protocol
    public const EWS  = 'ews';  // Microsoft Exchange Web Services
}
