<?php

namespace Beecubu\Foundation\Emailer\Entities\EmailConfig;

use Beecubu\Foundation\Core\Exceptions\SerializeIsNotSubclassOfSerializableException;
use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\Emailer\Entities\EmailEWS\EmailEWSVersion;
use Beecubu\Foundation\MongoDB\PlainEntity;

/**
 * Configura el envío de correos.
 *
 * @property string $service El tipus de servidor de correu (SMTP o Exchange).
 * @property string $from L'email de la persona que l'envia.
 * @property string $fromName El nom de la persona que l'envia.
 * @property string $localDomain El domini del servidor, exemple: [127.0.0.1]
 * @property string $server El servidor que envia el correu.
 * @property int $port El port del servidor que envia el correu.
 * @property string $userName El nom d'usuari que envia el correu.
 * @property string $password La contrasenya de l'usuari que envia el correu.
 * @property string $authMode El mètode d'autentificació.
 * @property string $encryption El mètode d'encriptació.
 * @property bool $verifyPeer TRUE = Comprova que sigui un certificat correcte, FALSE = permet certificats invàlids.
 * @property string $version La versió del Microsoft Exchange (VERSION_2007, VERSION_2009, VERSION_2010...)
 * @property bool $verified TRUE = El correu es pot fer servir. FALSE = nop.
 * @property string $verification El token de verificació, per comprovar que el correu funciona.
 *
 * @method bool hasLocalDomain()
 *
 * @method bool isVerified()
 */
class EmailConfig extends PlainEntity
{
    protected $ivar_isDefaultConfig = false;

    /** @var EmailConfig|null */
    protected static $defaultConfig = null;

    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'service'      => [Property::READ_WRITE, Property::IS_ENUM, EmailConfigService::class],
            // sender information
            'from'         => [Property::READ_WRITE, Property::IS_STRING],
            'fromName'     => [Property::READ_WRITE, Property::IS_STRING],
            // SMTP server configuration
            'localDomain'  => [Property::READ_WRITE, Property::IS_STRING],
            'server'       => [Property::READ_WRITE, Property::IS_STRING],
            'port'         => [Property::READ_WRITE, Property::IS_INTEGER],
            'userName'     => [Property::READ_WRITE, Property::IS_STRING],
            'password'     => [Property::READ_WRITE, Property::IS_STRING_ENCRYPTED],
            'authMode'     => [Property::READ_WRITE, Property::IS_ENUM, EmailConfigAuth::class],
            'encryption'   => [Property::READ_WRITE, Property::IS_ENUM, EmailConfigEncryption::class],
            'verifyPeer'   => [Property::READ_WRITE, Property::IS_BOOLEAN],
            // Exchange config
            'version'      => [Property::READ_WRITE, Property::IS_ENUM, EmailEWSVersion::class],
            // verification
            'verified'     => [Property::READ_ONLY, Property::IS_BOOLEAN],
            'verification' => [Property::READ_ONLY, Property::IS_STRING],
        ];
    }

    protected function excludedJsonProperties(): void
    {
        parent::excludedJsonProperties();
        // add some
        $this->excJsonProp = array_merge($this->excJsonProp, ['verification', 'password']);
    }

    // Override setters and getters

    protected function setLocalDomain(string $value): void
    {
        $value = trim(preg_replace('/([\[\]])/', '', $value));
        // set a clean localDomain
        $this->set_ivar('localDomain', empty($value) ? null : "[$value]");
    }

    // class constructors

    /**
     * Retorna el local domain. En cas de que l'usuari no hagi configurat res retorna [127.0.0.1]
     *
     * @return string El localDomain "correcte".
     */
    public function localDomain(): string
    {
        return $this->hasLocalDomain() ? $this->localDomain : '[127.0.0.1]';
    }

    /**
     * Genera (si fa falta) un nou codi de verificació i en el cas de canviar, marca com a no verificada la info.
     *
     * @return void
     *
     * @throws SerializeIsNotSubclassOfSerializableException
     */
    public function settingsChanged(): void
    {
        // generate the code verification
        $code = sha1(json_encode($this->json(['verified', 'verification'])));
        // code changed??
        if ($code !== $this->verification)
        {
            $this->set_ivar('verified', false);
            $this->set_ivar('verification', $code);
        }
    }

    /**
     * Verifica el token de verificació del correu.
     *
     * @param string $token El token a comprovar.
     *
     * @return void
     */
    public function verifyVerificationToken(string $token): void
    {
        if ( ! $this->verified)
        {
            $this->set_ivar('verified', $this->verification === $token);
        }
    }

    /**
     * Retorna si la config actual és la que hi ha per defecte.
     *
     * @return bool TRUE = Ho és, FALSE = no ho és... xD
     */
    public function isDefaultConfig(): bool
    {
        return $this->ivar_isDefaultConfig;
    }
}
