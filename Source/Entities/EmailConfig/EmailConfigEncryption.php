<?php

namespace Beecubu\Foundation\Emailer\Entities\EmailConfig;

use Beecubu\Foundation\Core\Enum;

class EmailConfigEncryption extends Enum
{
    public const Encryption_None = null;
    public const Encryption_TLS  = 'tls';
    public const Encryption_SSL  = 'ssl';
}
