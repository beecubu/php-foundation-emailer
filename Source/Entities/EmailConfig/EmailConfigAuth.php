<?php

namespace Beecubu\Foundation\Emailer\Entities\EmailConfig;

use Beecubu\Foundation\Core\Enum;

class EmailConfigAuth extends Enum
{
    public const PLAIN    = 'plain';
    public const LOGIN    = 'login';
    public const CRAM_MD5 = 'cram-md5';
    public const NULL     = 'null';
}
