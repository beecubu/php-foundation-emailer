<?php

namespace Beecubu\Foundation\Emailer\Entities\EmailQueueItem;

use Beecubu\Foundation\Core\Enum;

/**
 * Els diferents estats d'un email.
 */
class EmailQueueItemState extends Enum
{
    public const Waiting = 'waiting';
    public const Sending = 'sending';
    public const Sent    = 'sent';
    public const Error   = 'error';
}
