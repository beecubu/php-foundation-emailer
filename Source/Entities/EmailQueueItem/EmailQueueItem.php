<?php

namespace Beecubu\Foundation\Emailer\Entities\EmailQueueItem;

use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\Emailer\Entities\Email\EmailAttachment;
use Beecubu\Foundation\Emailer\Persistence\EmailQueueSenderDB;
use Beecubu\Foundation\MongoDB\Entity;
use DateTime;

/**
 * Un email de la cua d'emails.
 *
 * @property-read string $id L'id de l'item.
 * @property-read string $ownerId El propietari de l'item.
 * @property-read DateTime $date La data d'encuament.
 * @property string $state L'estat en que es troba l'item.
 * @property-read string $to El correu de a qui s'ha d'enviar.
 * @property-read string $toName El nom de a qui s'ha d'enviar.
 * @property-read string $replyTo A qui contestar el correu.
 * @property-read string $subject El títol del correu.
 * @property-read string $body El cos del correu.
 * @property-read EmailAttachment[] $attachments Els fitxers adjunts.
 * @property-read mixed $data Informació adicional que es vulgui guardar.
 * @property mixed $error El missatge d'error, o el backtrace pel qual ha fallat l'enviament.
 *
 * @method bool hasData()
 */
class EmailQueueItem extends Entity
{
    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'ownerId'     => [Property::READ_ONLY, Property::IS_STRING],
            'date'        => [Property::READ_ONLY, Property::IS_DATE],
            'state'       => [Property::READ_WRITE, Property::IS_ENUM, EmailQueueItemState::class],
            'to'          => [Property::READ_ONLY, Property::IS_STRING],
            'toName'      => [Property::READ_ONLY, Property::IS_STRING],
            'replyTo'     => [Property::READ_ONLY, Property::IS_STRING],
            'subject'     => [Property::READ_ONLY, Property::IS_STRING],
            'body'        => [Property::READ_ONLY, Property::IS_STRING],
            'attachments' => [Property::READ_ONLY, Property::IS_ARRAY, EmailAttachment::class],
            'data'        => [Property::READ_ONLY, Property::IS_MIXED],
            'error'       => [Property::READ_WRITE, Property::IS_MIXED],
        ];
    }

    /**
     * Obté la instància de la DB pels emails.
     *
     * @return EmailQueueSenderDB
     */
    protected function getEmailQueueSenderDB(): EmailQueueSenderDB
    {
        return EmailQueueSenderDB::current();
    }

    /**
     * Crea un nou correu preparat per ser enviat.
     *
     * @param string $ownerId El propietari.
     * @param string $to El correu electrònic on enviar.
     * @param string $toName El nom de la persona a enviar.
     * @param string $subject El títol del correu.
     * @param string $body El cos del correu.
     * @param EmailAttachment[] $attachments Els adjunts.
     * @param string|null $replyTo El reply to.
     *
     * @return static
     */
    public static function emailQueueItemWithInfo(string $ownerId, string $to, string $toName, string $subject, string $body, array $attachments, ?string $replyTo): self
    {
        return static::emailQueueItemWithDataInfo(null, $ownerId, $to, $toName, $subject, $body, $attachments, $replyTo);
    }

    /**
     * Crea un nou correu amb informació adicional preparat per ser enviat.
     *
     * @param mixed $data Informació adicional.
     * @param string $ownerId El propietari.
     * @param string $to El correu electrònic on enviar.
     * @param string $toName El nom de la persona a enviar.
     * @param string $subject El títol del correu.
     * @param string $body El cos del correu.
     * @param EmailAttachment[] $attachments Els adjunts.
     * @param string|null $replyTo El reply to.
     *
     * @return static
     */
    public static function emailQueueItemWithDataInfo($data, string $ownerId, string $to, string $toName, string $subject, string $body, array $attachments, ?string $replyTo): self
    {
        $item = new self();
        // set ivars
        $item->set_ivar('data', $data);
        $item->set_ivar('ownerId', $ownerId);
        $item->set_ivar('date', new DateTime());
        $item->set_ivar('state', EmailQueueItemState::Waiting);
        $item->set_ivar('to', $to);
        $item->set_ivar('toName', $toName);
        $item->set_ivar('replyTo', $replyTo);
        $item->set_ivar('subject', $subject);
        $item->set_ivar('body', $body);
        $item->set_ivar('attachments', $attachments);
        // the item
        return $item;
    }

    /**
     * Crea un nuevo correo preparado para ser enviado y lo guarda automáticamente.
     *
     * @param string $ownerId El propietari.
     * @param string $to El email donde enviar.
     * @param string $toName El nombre de la persona a enviar.
     * @param string $subject El título del correo.
     * @param string $body El cuerpo del correo.
     * @param EmailAttachment[] $attachments Los adjuntos.
     * @param string|null $replyTo El reply to.
     */
    public static function createEmailQueueItemWithInfo(string $ownerId, string $to, string $toName, string $subject, string $body, array $attachments, ?string $replyTo): void
    {
        static::emailQueueItemWithInfo($ownerId, $to, $toName, $subject, $body, $attachments, $replyTo)->save();
    }

    /**
     * Assigna el reply to.
     *
     * @param string $replyTo El reply to.
     */
    public function setReplyTo(string $replyTo): void
    {
        $this->set_ivar('replyTo', $replyTo);
    }

    /**
     * Guarda els canvis.
     */
    public function save(): void
    {
        $this->getEmailQueueSenderDB()->saveEmailQueueItem($this);
    }

    /**
     * Elimina l'email.
     */
    public function delete(): void
    {
        $this->getEmailQueueSenderDB()->deleteEmailQueueItem($this);
    }
}
