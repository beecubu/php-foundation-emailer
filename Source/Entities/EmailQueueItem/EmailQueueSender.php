<?php

namespace Beecubu\Foundation\Emailer\Entities\EmailQueueItem;

use Beecubu\Foundation\Emailer\Entities\Email\Email;
use Beecubu\Foundation\Emailer\Entities\EmailConfig\EmailConfig;
use Beecubu\Foundation\Emailer\Entities\Exceptions\EmailOwnerIdIsNotFoundOrIsNotOwnerUserException;
use Beecubu\Foundation\Emailer\Persistence\EmailQueueSenderDB;
use Throwable;

/**
 * Envia els correus que estan en cua.
 */
abstract class EmailQueueSender
{
    /**
     * Obté la config a fer servir per enviar un correu.
     *
     * @param EmailQueueItem $item El correu.
     *
     * @return Email
     *
     * @throws EmailOwnerIdIsNotFoundOrIsNotOwnerUserException
     */
    protected static function getEmailSender(EmailQueueItem $item): Email
    {
        static $cachedMailers = [];
        // not used before? then create it
        if ( ! isset($cachedMailers[$item->ownerId]))
        {
            if ($emailConfig = static::getEmailConfigByItem($item))
            {
                $cachedMailers[$item->ownerId] = Email::emailWithEmailConfig($emailConfig);
            }
            else // ops!! owner user do not exists or is not an owner user
            {
                throw new EmailOwnerIdIsNotFoundOrIsNotOwnerUserException();
            }
        }
        return $cachedMailers[$item->ownerId];
    }

    /**
     * Obté la configuració de correu per un EmailQueueItem concret.
     *
     * @param EmailQueueItem $item L'item de referència.
     *
     * @return EmailConfig|null
     */
    abstract protected static function getEmailConfigByItem(EmailQueueItem $item): ?EmailConfig;

    /**
     * Obté la instància de la DB pels emails.
     *
     * @return EmailQueueSenderDB
     */
    protected static function getEmailQueueSenderDB(): EmailQueueSenderDB
    {
        return EmailQueueSenderDB::current();
    }

    /**
     * Envia tots els emails que hi ha en cua.
     * <b>Nota: <b/>Aquest procés té en compte que hi poden haver diferents fils treballant.
     */
    public static function execute(): void
    {
        /** @type EmailQueueItem $queuedEmail */
        while ($queuedEmail = static::getEmailQueueSenderDB()->firstEmailQueueItemWithState(EmailQueueItemState::Waiting))
        {
            try
            {
                // send the email
                $email = self::getEmailSender($queuedEmail);
                // change the email state to "sending" (lock it from other threads)
                $queuedEmail->state = EmailQueueItemState::Sending;
                $queuedEmail->save();
                // try to send the email
                $email->sendQueuedEmail($queuedEmail);
                // all ok, so remove it from queue
                $queuedEmail->delete();
            }
            catch (Throwable $e)
            {
                $queuedEmail->state = EmailQueueItemState::Error;
                $queuedEmail->error = $e->getMessage();
                $queuedEmail->save();
            }
        }
    }
}
