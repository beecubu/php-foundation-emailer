<?php

namespace Beecubu\Foundation\Emailer\Entities\Exceptions;

use Exception;

/**
 * Quan falla enviant un correu usant el Microsoft Exchange.
 */
class EmailEWSSendingError extends Exception
{
    public function __construct($errors)
    {
        parent::__construct(implode("\n", $errors));
    }
}
