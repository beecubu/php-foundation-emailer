<?php

namespace Beecubu\Foundation\Emailer\Entities\Exceptions;

use Exception;

/**
 * Quan el ownerId no existeix o no es d'un owner id.
 */
class EmailOwnerIdIsNotFoundOrIsNotOwnerUserException extends Exception
{
    public function __construct()
    {
        parent::__construct('The email owner id is not and owner user or do not exist.');
    }
}
