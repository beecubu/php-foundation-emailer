<?php

namespace Beecubu\Foundation\Emailer\Entities\Exceptions;

use Exception;

/**
 * Cuando intenta enviar un correo que está en la cola y el correo no existe.
 */
class EmailQueueItemAndEmailOwnerAreNotEqualException extends Exception
{
    public function __construct()
    {
        parent::__construct('The email queue and owner email are not equal.');
    }
}
